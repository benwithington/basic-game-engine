CC = g++
CXXFLAGS = -std=c++20 -Wall -g
LDLIBS = -lsfml-graphics -lsfml-window -lsfml-system -lsfml-audio

SRC_DIR = src/
BUILD_DIR = build/
BIN_DIR = bin/

SRCS = $(wildcard $(SRC_DIR)*.cpp)
OBJS = $(patsubst $(SRC_DIR)%.cpp, $(BUILD_DIR)%.o, $(SRCS))

TARGET = $(BIN_DIR)app

$(TARGET): $(OBJS)
	$(CC) $(CXXFLAGS) $^ -o $@ $(LDLIBS)

$(BUILD_DIR)%.o: $(SRC_DIR)%.cpp
	$(CC) $(CXXFLAGS) -c $< -o $@

.PHONY: clean
clean:
	rm -f $(OBJS) $(TARGET)
