#include "Game.h"
#include <iostream>
#include <cmath>
#include <numbers>
#include <sstream>

Game::Game() : gen(std::random_device{}()) {
    init();
}

void Game::init() {
    //TODO: setup PlayerConfig, EnemyConfig, BulletConfig
    this->playerConfig  = { 
        32, 32,         // Radius
        5,              // Speed
        5, 5, 5,        // Fill Colour 
        255, 0, 0,      // Outline Colour 
        4,              // Outline Thickness
        8               // Vertices
    };
    this->enemyConfig   = { 
        32, 32,         // Radius 
        255, 255, 255,  // Outline Colour 
        2,              // Outline Thickness 
        3, 8,           // VMIN, VMAX (Vertices) 
        90,             // Small Lifespan 
        120,            // Spawn Interval    
        3,  10           // SMIN, SMAX  (SPEED) 
    };
    this->bulletConfig  = { 
        10, 10,         // Radius
        20,             // Speed
        255, 255, 255,  // Fill Colour
        255, 255, 255,  // Outline Colour
        2,              // Outline Thickness
        20,             // Vertices
        90              // Lifespan
    };

    window.create(sf::VideoMode(1280, 720), "SMFL Engine");
    window.setFramerateLimit(60);
    window.setPosition(sf::Vector2i(3100, 520));

    if(!this->font.loadFromFile("res/fonts/Roboto-Regular.ttf")) {
        std::cout << "Could no load font from file\n";
        this->running = false;
    }
    
    this->text.setFont(this->font);
    this->text.setCharacterSize(36);
    this->text.setStyle(sf::Text::Regular);
    this->text.setPosition(10, 10);

    spawnPlayer();
}

void Game::run() {
    while(running) {
        if(this->reset) {
            this->restart();
        }

        this->entities.update();

        if(!this->paused) {
            this->enemySpawner();
            this->collision();
            this->movement();
            this->lifespan();
        }

        this->userInput();
        this->render();

        ++currentFrame;
    }
}

void Game::restart() {
    for(auto e : entities.getEntities()) {
        e->destroy();
    }

    spawnPlayer();

    if(this->score > this->highScore) {
        this->highScore = this->score;
    }
    this->score = 0;
    this->currentFrame = 0;
    this->lastEnemySpawnFrame = 0;  
    this->reset = false;
}

void Game::setPaused(bool paused) {
    this->paused = paused;
}

void Game::spawnPlayer() {
    auto player = this->entities.addEntity("player");
    auto vp = window.getSize();
    player->transform = std::make_shared<Component::Transform>(
        Vec2(vp.x / 2.0f, vp.y / 2.0f), 
        Vec2(0.0f, 0.0f),
        0.0f
    );
    player->shape = std::make_shared<Component::Shape>(
        playerConfig.SR, 
        playerConfig.V, 
        sf::Color(playerConfig.FR, playerConfig.FG, playerConfig.FB), 
        sf::Color(playerConfig.OR, playerConfig.OG, playerConfig.OB), 
        playerConfig.OT
    );
    player->collision = std::make_shared<Component::Collision>(
        playerConfig.CR
    );
    player->input = std::make_shared<Component::Input>();

    player->health = std::make_shared<Component::Health>(3);

    this->player = player;
}

void Game::spawnEnemy() {

    //Spawn enemy here using config within bounds of the window
    auto windowSize = this->window.getSize();
    auto width = windowSize.x;
    auto height = windowSize.y;

    int spawnX = this->getRandomNumber<int>(enemyConfig.SR, width  - enemyConfig.SR);
    int spawnY = this->getRandomNumber<int>(enemyConfig.SR, height - enemyConfig.SR);

    int vertices = this->getRandomNumber<int>(enemyConfig.VMIN, enemyConfig.VMAX);
    int speed    = this->getRandomNumber<int>(enemyConfig.SMIN, enemyConfig.SMAX);

    auto enemy = this->entities.addEntity("enemy");
    enemy->shape = std::make_shared<Component::Shape>(
        enemyConfig.SR,
        vertices,
        sf::Color(
            this->getRandomNumber<int>(0, 255),
            this->getRandomNumber<int>(0, 255),
            this->getRandomNumber<int>(0, 255)
        ),
        sf::Color(enemyConfig.OR, enemyConfig.OG, enemyConfig.OB),
        enemyConfig.OT
    );

    float angle = this->getRandomNumber<float>(0.0f, std::numbers::pi_v<float>);
    enemy->transform = std::make_shared<Component::Transform>(
        Vec2{static_cast<float>(spawnX), static_cast<float>(spawnY)},
        Vec2{speed * std::sin(angle), speed * std::cos(angle)},
        0.0f
    );
    enemy->collision = std::make_shared<Component::Collision>(enemyConfig.CR);

    enemy->score = std::make_shared<Component::Score>(speed);

    this->lastEnemySpawnFrame = this->currentFrame;
}

void Game::spawnSmallEnemies(std::shared_ptr<Entity> enemy) {
    // Spawn small enemies at location of input enemy
    std::size_t vertices = enemy->shape->circle.getPointCount();

    for(std::size_t i = 0; i < vertices; ++i) {
        auto smallEnemy = this->entities.addEntity("small");

        smallEnemy->shape = std::make_shared<Component::Shape>(
            enemy->shape->circle.getRadius() / 2.0f,
            vertices,
            enemy->shape->circle.getFillColor(),
            enemy->shape->circle.getOutlineColor(),
            enemy->shape->circle.getOutlineThickness()
        );

        float angle = i * ((2 * std::numbers::pi) / vertices);
        float speed = enemy->transform->velocity.x / std::cos(std::atan2(enemy->transform->velocity.y, enemy->transform->velocity.x));
        smallEnemy->transform = std::make_shared<Component::Transform>(
            enemy->transform->pos,
            Vec2{
                speed * std::sin(angle), 
                speed * std::cos(angle)
            },
            enemy->transform->angle
        );

        smallEnemy->score = std::make_shared<Component::Score>(
            enemy->score->score * 2
        );

        smallEnemy->collision = std::make_shared<Component::Collision>(
            enemy->collision->radius / 2.0f
        );

        smallEnemy->lifespan = std::make_shared<Component::Lifespan>(enemyConfig.L);
    }  
}

void Game::spawnBullet(std::shared_ptr<Entity> entity, const Vec2& target) {
    // Bullet travels towards target from entity
    // Bullet speed is a scalar value
    auto bullet = this->entities.addEntity("bullet");

    bullet->shape = std::make_shared<Component::Shape>(
        bulletConfig.SR,
        bulletConfig.V,
        sf::Color(bulletConfig.OR, bulletConfig.OG, bulletConfig.OB),
        sf::Color(bulletConfig.OR, bulletConfig.OG, bulletConfig.OB),
        bulletConfig.OT
    );
    bullet->collision = std::make_shared<Component::Collision>(
        bulletConfig.CR
    );
    bullet->lifespan = std::make_shared<Component::Lifespan>(
        bulletConfig.L
    );

    Vec2 normalisedDiff{(target - entity->transform->pos).normalise()};
    bullet->transform = std::make_shared<Component::Transform>(
        entity->transform->pos,
        Vec2{
            bulletConfig.S * normalisedDiff.x,
            bulletConfig.S * normalisedDiff.y
        },
        0.0f
    );
}

void Game::movement() {

    // Player movement
    auto playerInputs = this->player->input;

    Vec2 playerVelocity{0.0f, 0.0f};
    if(playerInputs->up)    playerVelocity.y -= this->playerConfig.S;
    if(playerInputs->down)  playerVelocity.y += this->playerConfig.S;
    if(playerInputs->right) playerVelocity.x += this->playerConfig.S;
    if(playerInputs->left)  playerVelocity.x -= this->playerConfig.S;

    // Stop Movement from going off screen
    auto windowSize = this->window.getSize();

    for(auto enemy : entities.getEntities("enemy")) {
        if(enemy->transform->pos.x < 0 || enemy->transform->pos.x > windowSize.x) {
            enemy->transform->velocity.x = -enemy->transform->velocity.x;
        }
        if(enemy->transform->pos.y < 0 || enemy->transform->pos.y > windowSize.y) {
            enemy->transform->velocity.y = -enemy->transform->velocity.y;
        }
    }

    for(auto smallEnemy : entities.getEntities("small")) {
        if(smallEnemy->transform->pos.x < 0 || smallEnemy->transform->pos.x > windowSize.x) {
            smallEnemy->transform->velocity.x = -smallEnemy->transform->velocity.x;
        }
        if(smallEnemy->transform->pos.y < 0 || smallEnemy->transform->pos.y > windowSize.y) {
            smallEnemy->transform->velocity.y = -smallEnemy->transform->velocity.y;
        }
    }

    this->player->transform->velocity = playerVelocity;

    // Update movement for all entities with a transform
    for(auto entity : entities.getEntities()) {
        if(entity->transform) {
            entity->transform->pos += entity->transform->velocity;
        }
    }

    // clamp players position
    float playerRadius = this->player->collision->radius;
    Vec2 playerMax{
        windowSize.x - playerRadius, 
        windowSize.y - playerRadius
    };
    Vec2 playerMin{
        0 + playerRadius, 
        0 + playerRadius
    };
    auto clamp = [](auto min, auto max, auto value) {
        return std::min(std::max(value, min), max);
    };

    this->player->transform->pos.x = clamp(playerMin.x, playerMax.x, this->player->transform->pos.x);
    this->player->transform->pos.y = clamp(playerMin.y, playerMax.y, this->player->transform->pos.y);

}

void Game::lifespan() {
    for(auto entity : entities.getEntities()) {
        if(!entity->lifespan) continue;

        if(entity->lifespan->remaining > 0)
            entity->lifespan->remaining--;

        if(entity->lifespan->remaining <= 0) {
            entity->destroy();
            continue;
        }

        if(entity->shape) {
            float proportion = static_cast<float>(entity->lifespan->remaining) / 
                                static_cast<float>(entity->lifespan->total);

            int alpha = static_cast<int>(proportion * 255.0f);

            auto colour = entity->shape->circle.getFillColor();
            entity->shape->circle.setFillColor(
                sf::Color(colour.r, colour.g, colour.b, alpha)
            );
            colour = entity->shape->circle.getOutlineColor();
            entity->shape->circle.setOutlineColor(
                sf::Color(colour.r, colour.g, colour.b, alpha)
            );
        }
    }   
}

void Game::userInput() {
    sf::Event event;
    while(this->window.pollEvent(event)) {
        if(event.type == sf::Event::Closed) {
            close();
        }

        if(event.type == sf::Event::KeyPressed) {
            if(event.key.code == sf::Keyboard::Escape) {
                close();
            }

            if(event.key.code == sf::Keyboard::P) {
                this->setPaused(!this->paused);
            }
        }

        if(event.type == sf::Event::MouseButtonPressed && !this->paused) {
            if(event.mouseButton.button == sf::Mouse::Left) {
                auto mousePos = sf::Mouse::getPosition(this->window);
                this->spawnBullet(this->player, Vec2(mousePos.x, mousePos.y));
            }

            if(event.mouseButton.button == sf::Mouse::Right) {
                this->spawnSpecialWeapon(this->player);
            }
        }
    }
    this->player->input->left   = sf::Keyboard::isKeyPressed(sf::Keyboard::A);
    this->player->input->right  = sf::Keyboard::isKeyPressed(sf::Keyboard::D);
    this->player->input->up     = sf::Keyboard::isKeyPressed(sf::Keyboard::W);
    this->player->input->down   = sf::Keyboard::isKeyPressed(sf::Keyboard::S);
}

void Game::render() {
    this->window.clear(sf::Color::Black);

    for(auto e : this->entities.getEntities()) {
        if(e->transform && e->shape) {
            e->shape->circle.setPosition(
                e->transform->pos.x, 
                e->transform->pos.y
            );
            e->transform->angle += 1.0f;
            e->shape->circle.setRotation(e->transform->angle);
            window.draw(e->shape->circle);
        }
    }

    std::stringstream ss{};
    ss << "High Score: " << this->highScore
        << "\nScore: "   << this->score 
        << "\nHealth: "  << this->player->health->health;
    this->text.setString(ss.str());
    window.draw(this->text);

    this->window.display();
}

void Game::enemySpawner() {
    int lastSpawnDelta = this->currentFrame - this->lastEnemySpawnFrame;
    if(enemyConfig.SI < lastSpawnDelta) {
        this->spawnEnemy();
    }
}

void Game::collision() {
    const auto isCollision = [](std::shared_ptr<Entity> e1, std::shared_ptr<Entity> e2) {

        float distanceSquared   = e1->transform->pos.distanceSquared(e2->transform->pos);
        float radiusSum         = e1->collision->radius + e2->collision->radius;
        float radiusSumSquared  = radiusSum * radiusSum;

        return distanceSquared < radiusSumSquared;
    };

    for(auto enemy : entities.getEntities("enemy")) {
        if(isCollision(this->player, enemy)) {
            this->player->health->health -= 3;
            if(this->player->health->health <= 0) {
                this->reset = true;
            }
            enemy->destroy();
        }
    }

    for(auto smallEnemy : entities.getEntities("small")) {
        if(isCollision(this->player, smallEnemy)) {
            this->player->health->health -= 1;
            if(this->player->health->health <= 0) {
                this->reset = true;
            }
            smallEnemy->destroy();
        }
    }

    for(auto bullet : entities.getEntities("bullet")) {
        for(auto enemy : entities.getEntities("enemy")) {

            if(isCollision(enemy, bullet)) {
                this->score += enemy->score->score;
                this->spawnSmallEnemies(enemy);
                enemy->destroy();
                bullet->destroy();
            }
        }

        for(auto smallEnemy : entities.getEntities("small")) {

            if(isCollision(smallEnemy, bullet)) {
                this->score += smallEnemy->score->score;
                smallEnemy->destroy();
                bullet->destroy();
            }
        }
    }
}

void Game::spawnSpecialWeapon(std::shared_ptr<Entity> entity) {

}

void Game::close() {
    this->window.close();
    this->running = false;
}

template<typename T>
T Game::getRandomNumber(T min, T max) {
    static_assert(std::is_arithmetic<T>::value, "Type T must be numeric");

    if constexpr (std::is_integral<T>::value) {
        return std::uniform_int_distribution<T>{min, max}(this->gen);
    } else {
        return std::uniform_real_distribution<T>{min, max}(this->gen);
    }
}
