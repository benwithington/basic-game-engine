 #ifndef ENTITY_H
#define ENTITY_H

#include <string>
#include <memory>

#include "Components.h"

class Entity {
    const std::string   tag{"Default"};
    const std::size_t   id{0};
    bool                active{true};
    Entity(const std::string& tag, std::size_t id);
    friend class EntityManager;
public:
    std::shared_ptr<Component::Transform>   transform;
    std::shared_ptr<Component::Shape>       shape;
    std::shared_ptr<Component::Collision>   collision;
    std::shared_ptr<Component::Input>       input;
    std::shared_ptr<Component::Score>       score;
    std::shared_ptr<Component::Lifespan>    lifespan;
    std::shared_ptr<Component::Health>      health;

    void destroy();
    bool isActive() const;
    const std::string& getTag() const;
    const std::size_t getId() const;
};

#endif // ENTITY_H