#include "EntityManager.h"
#include <algorithm>

EntityManager::EntityManager() {}

std::shared_ptr<Entity> EntityManager::addEntity(const std::string& tag) {
    auto entity = std::shared_ptr<Entity>(new Entity(tag, this->totalEntities++));
    this->toAdd.push_back(entity);
    return entity;
}

EntityVector& EntityManager::getEntities() {
    return this->entities;
}

EntityVector& EntityManager::getEntities(const std::string& tag) {
    return this->entityMap[tag];
}

void EntityManager::update() {
    for(auto e : this->toAdd) {
        this->entities.push_back(e);
        this->entityMap[e->getTag()].push_back(e);
    }
    this->toAdd.clear();

    auto it = this->entities.begin();
    while(it != this->entities.end()) {
        if(!(*it)->active) {
            auto& entityVec = this->entityMap[(*it)->getTag()];
            auto entityIterator = std::find(entityVec.begin(), entityVec.end(), *it);
            if(entityIterator != entityVec.end()) {
                entityVec.erase(entityIterator);
            }

            it = this->entities.erase(it);
        } else {
            ++it;
        }
    }
}

