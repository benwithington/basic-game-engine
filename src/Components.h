#ifndef COMPONENTS_H
#define COMPONENTS_H

#include <SFML/Graphics.hpp>
#include <iostream>
#include "Vec2.h"

namespace Component {

    class Transform {
    public:
        Vec2 pos{ 0.0f, 0.0f };
        Vec2 velocity{ 0.0f, 0.0f };
        float angle{ 0.0f };

        Transform(const Vec2& p, const Vec2& v, float a) 
            : pos(p), velocity(v), angle(a)
        {}
    };

    class Shape {
    public:
        sf::CircleShape circle{};

        Shape(float radius, int points, const sf::Color& fill, const sf::Color& outline, float thickness) 
            : circle(radius, points)
        {
            circle.setFillColor(fill);
            circle.setOutlineColor(outline);
            circle.setOutlineThickness(thickness);
            circle.setOrigin(radius, radius);
        }
    };

    class Collision {
    public:
        float radius{ 0.0f };

        Collision(float r) 
            : radius(r)
        {}
    };

    class Input {
    public:
        bool up     { false };
        bool left   { false };
        bool right  { false };
        bool down   { false };
        bool shoot  { false };

        Input() {}
    };

    class Score {
    public:
        int score{ 0 };

        Score(int s) 
            : score(s)
        {}
    };

    class Lifespan {
    public:
        int remaining{0};
        int total{0};

        Lifespan(int total)
            : remaining(total), total(total)
        {}
    };

    class Health {
    public:
        int health{0};

        Health(int health)
            : health(health)
        {}
    };    
}

#endif // COMPONENTS_H