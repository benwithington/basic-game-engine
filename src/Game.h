#ifndef GAME_H
#define GAME_H

#include <random>
#include <memory>
#include <SFML/Graphics.hpp>
#include "EntityManager.h"
#include "Entity.h"


struct PlayerConfig { int SR, CR, S, FR, FG, FB, OR, OG, OB, OT; float V; };
struct BulletConfig { int SR, CR, S, FR, FG, FB, OR, OB, OG, OT, V, L; };
struct EnemyConfig  { int SR, CR, OR, OG, OB, OT, VMIN, VMAX, L, SI; float SMIN, SMAX; };

class Game {
    sf::RenderWindow window;
    EntityManager entities;
    sf::Font font;
    sf::Text text;
    PlayerConfig playerConfig;
    EnemyConfig enemyConfig;
    BulletConfig bulletConfig;
    std::shared_ptr<Entity> player;
    int highScore{ 0 };
    int score{ 0 };
    int currentFrame{ 0 };
    int lastEnemySpawnFrame{ 0 };
    bool paused{ false };
    bool running{ true };
    bool reset{ false };

    std::mt19937 gen;

    void init();
    void setPaused(bool paused);

    //Systems
    void movement();
    void userInput();
    void render();
    void enemySpawner();
    void collision();
    void lifespan();

    void restart();
    void spawnPlayer();
    void spawnEnemy();
    void spawnSmallEnemies(std::shared_ptr<Entity> entity);
    void spawnBullet(std::shared_ptr<Entity> entity, const Vec2& mousePos);
    void spawnSpecialWeapon(std::shared_ptr<Entity> entity);
    void close();

public:
    Game();

    void run();
    template<typename T>
    T getRandomNumber(T min, T max);
};

#endif