#ifndef VEC2_H
#define VEC2_H

class Vec2 {
public:
    float x = 0;
    float y = 0;
    
    Vec2() {}
    Vec2(float x, float y);

    bool operator==(const Vec2& v) const;
    bool operator!=(const Vec2& v) const;
    Vec2 operator+(const Vec2& v) const;
    Vec2 operator-(const Vec2& v) const;
    Vec2 operator*(const float scale) const;
    Vec2 operator/(const float scale) const;
    void operator+=(const Vec2& v);
    void operator-=(const Vec2& v);
    void operator*=(const float value);
    void operator/=(const float value);

    Vec2& scale(float scale);
    Vec2& add(float value);

    float length() const;
    float distance(const Vec2& v) const;
    float distanceSquared(const Vec2& v) const;
    Vec2 normalise() const;  

    void print() const;
};

std::ostream& operator<< (std::ostream& out, const Vec2& v);

#endif // VEC2_H