#include "Entity.h"

Entity::Entity(const std::string& tag, std::size_t id) : tag(tag), id(id) {}

void Entity::destroy() {
    this->active = false;
}

bool Entity::isActive() const {
    return this->active;
}

const std::string& Entity::getTag() const {
    return this->tag;
}

const std::size_t Entity::getId() const {
    return this->id;
}