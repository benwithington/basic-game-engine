#include <iostream>
#include <math.h>
#include "Vec2.h"

Vec2::Vec2(float x, float y)
    : x(x), y(y)
{}

bool Vec2::operator==(const Vec2& v) const {
    return this->x == v.x && this->y == v.y;
}

bool Vec2::operator!=(const Vec2& v) const {
    return !(*this == v);
}

Vec2 Vec2::operator+(const Vec2& v) const {
    return Vec2(this->x + v.x, this->y + v.y);
}

Vec2 Vec2::operator-(const Vec2& v) const {
    return Vec2(this->x - v.x, this->y - v.y);
}

Vec2 Vec2::operator*(const float scale) const {
    return Vec2(this->x * scale, this->y * scale);
}

Vec2 Vec2::operator/(const float scale) const {
    return Vec2(this->x / scale, this->y / scale);
}

void Vec2::operator+=(const Vec2& v) {
    this->x += v.x;
    this->y += v.y;
}

void Vec2::operator-=(const Vec2& v) {
    this->x -= v.x;
    this->y -= v.y;
}

void Vec2::operator*=(const float value) {
    this->x *= value;
    this->y *= value;
}

void Vec2::operator/=(const float value) {
    this->x /= value;
    this->y /= value;
}

Vec2& Vec2::add(const float value) {
    this->x += value;
    this->y += value;
    return *this;
}

Vec2& Vec2::scale(const float sf) {
    this->x *= sf;
    this->y *= sf;
    return *this;
}

float Vec2::distance(const Vec2& v) const {
    return std::sqrt(distanceSquared(v));
}

float Vec2::distanceSquared(const Vec2& v) const {
    Vec2 diffVec = *this - v;
    return diffVec.x * diffVec.x + diffVec.y * diffVec.y;
}

float Vec2::length() const {
    return this->distance(Vec2{0.0f, 0.0f});
}

Vec2 Vec2::normalise() const {
    return *this / this->length();
}

void Vec2::print() const {
    std::cout << x << " " << y << '\n';
}

std::ostream& operator<< (std::ostream& out, const Vec2& v) {
    out << "Vec2(" << v.x << ", " << v.y << ')';
    return out;
}
